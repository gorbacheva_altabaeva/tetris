package com.mygdx.tetris;

import com.badlogic.gdx.math.Rectangle;

class Shape {
    private Rectangle rectangle;
    private ShapeType shapeType;

    Shape(Rectangle rectangle, ShapeType shapeType) {
        this.rectangle = rectangle;
        this.shapeType = shapeType;
    }

    Rectangle getRectangle() {
        return rectangle;
    }

    ShapeType getShapeType() {
        return shapeType;
    }

    void setRectangle(Rectangle rectangle) {
        this.rectangle = rectangle;
    }

    /**
     * Возвращает фигуру, которая приобрела необходимые после падения координаты
     *
     * @param dropShapes - падающая фигура
     * @return фигура, которая приобрела необходимые после падения координаты
     */
    Shape[] saveShape(Shape[] dropShapes) {
        Rectangle[] rectangles = {
                dropShapes[0].getRectangle(),
                dropShapes[1].getRectangle(),
                dropShapes[2].getRectangle(),
                dropShapes[3].getRectangle()
        };
        boolean isVerticalStrip = dropShapes[0].getShapeType() == ShapeType.Strip
                && rectangles[0].x == rectangles[1].x;
        for (int i = 0; i < dropShapes.length; i++) {
            Rectangle rectangle = dropShapes[i].getRectangle();
            ShapeType shapeType = dropShapes[i].getShapeType();
            switch (shapeType) {
                case Square: {
                    switch (i) {
                        case 0:
                        case 1: {
                            rectangle.y = this.rectangle.y + 32;
                            break;
                        }
                        case 2:
                        case 3: {
                            rectangle.y = this.rectangle.y + 64;
                            break;
                        }
                    }
                    GameScreen.array[(int) rectangle.y / 32 - 3]++;
                    dropShapes[i].setRectangle(rectangle);
                    break;
                }
                case Strip: {
                    if (isVerticalStrip) {
                        switch (i) {
                            case 0: {
                                rectangle.y = this.rectangle.y + 128;
                                break;
                            }
                            case 1: {
                                rectangle.y = this.rectangle.y + 96;
                                break;
                            }
                            case 2: {
                                rectangle.y = this.rectangle.y + 64;
                                break;
                            }
                            case 3: {
                                rectangle.y = this.rectangle.y + 32;
                                break;
                            }
                        }
                    } else {
                        rectangle.y = this.rectangle.y + 32;
                    }

                    GameScreen.array[(int) rectangle.y / 32 - 3]++;
                    dropShapes[i].setRectangle(rectangle);
                    break;
                }
                default: {
                    break;
                }
            }
        }

        return dropShapes;
    }
}
