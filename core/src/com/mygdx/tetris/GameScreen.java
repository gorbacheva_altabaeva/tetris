package com.mygdx.tetris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
// import com.badlogic.gdx.audio.Music;
// import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

public class GameScreen implements Screen {
    private final Tetris game;
    private static final int SCREEN_WIDTH = 480;
    private static final int SCREEN_HEIGHT = 800;
    private Texture squareImage;
    private Texture stripImage;
    private Texture saved;
    // private Sound dropSound;
    // private Music backgroundMusic;
    private OrthographicCamera camera;
    private Array<Shape[]> dropShapes;
    private Array<Shape> savedShapes;
    //private long lastDropTime;
    private int fullLines;
    private Texture backgroundTexture;
    private Sprite backgroundSprite;


    static int[] array = new int[]{
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    GameScreen(final Tetris tetris) {
        this.game = tetris;
        backgroundTexture = new Texture(Gdx.files.internal("background.png"));
        backgroundSprite = new Sprite(backgroundTexture);
        backgroundSprite.setSize(SCREEN_WIDTH, SCREEN_HEIGHT);
        backgroundSprite.setPosition(0, 0f);

        saved = new Texture(Gdx.files.internal("yellow.png"));
        squareImage = new Texture(Gdx.files.internal("blue.png"));
        stripImage = new Texture(Gdx.files.internal("red.png"));

        // загрузка звукового эффекта падения
        // dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
        // загрузка фоновой музыки
        // backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("имя файла.mp3"));
        // зацикливаем музыку
        // backgroundMusic.setLooping(true);

        camera = new OrthographicCamera();
        camera.setToOrtho(false, SCREEN_WIDTH, SCREEN_HEIGHT);
        dropShapes = new Array<>();
        savedShapes = new Array<>();
        savedShapes.add(new Shape(new Rectangle(0, 64, SCREEN_WIDTH, 32), ShapeType.Default));
        shapeGeneration();
    }


    /**
     * Генерация падающей фигуры
     */
    private void shapeGeneration() {
        int type = (int) (Math.random() * 2);
        switch (type) {
            case 0: {
                stripGeneration();
                break;
            }
            default: {
                squareGeneration();
                break;
            }
        }
    }

    /**
     * Генерация квадрата
     */
    private void squareGeneration() {
        Shape[] squaresDrop = new Shape[4];

        squaresDrop[0] = new Shape(new Rectangle(208, SCREEN_HEIGHT - 32, 32, 32), ShapeType.Square);
        squaresDrop[1] = new Shape(new Rectangle(240, SCREEN_HEIGHT - 32, 32, 32), ShapeType.Square);
        squaresDrop[2] = new Shape(new Rectangle(208, SCREEN_HEIGHT, 32, 32), ShapeType.Square);
        squaresDrop[3] = new Shape(new Rectangle(240, SCREEN_HEIGHT, 32, 32), ShapeType.Square);

        dropShapes.add(squaresDrop);
    }

    /**
     * Генерация полоски
     */
    private void stripGeneration() {
        Shape[] squaresDrop = new Shape[4];

        squaresDrop[0] = new Shape(new Rectangle(176, SCREEN_HEIGHT, 32, 32), ShapeType.Strip);
        squaresDrop[1] = new Shape(new Rectangle(208, SCREEN_HEIGHT, 32, 32), ShapeType.Strip);
        squaresDrop[2] = new Shape(new Rectangle(240, SCREEN_HEIGHT, 32, 32), ShapeType.Strip);
        squaresDrop[3] = new Shape(new Rectangle(272, SCREEN_HEIGHT, 32, 32), ShapeType.Strip);

        dropShapes.add(squaresDrop);
    }

    private void drawShapes(Shape shape) {
        ShapeType shapeType = shape.getShapeType();

        if (shapeType == ShapeType.Default) {
            return;
        }

        Texture texture;

        switch (shapeType) {
            case Square: {
                texture = squareImage;
                break;
            }
            case Strip: {
                texture = stripImage;
                break;
            }
            default: {
                return;
            }
        }

        Rectangle rectangle = shape.getRectangle();
        game.batch.draw(texture, rectangle.x, rectangle.y);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        camera.update();
        game.batch.setProjectionMatrix(camera.combined);
        game.batch.begin();
        backgroundSprite.draw(game.batch);
        //game.font.draw(game.batch, "Score: " + fullLines, 200, 800);

        // рисуем сохраненные фигуры
        for (Shape shape : savedShapes) {
            drawShapes(shape);
        }

        // рисуем падающие фигуры
        for (Shape shape : dropShapes.get(0)) {
            drawShapes(shape);
        }

        game.batch.end();

        // панель управления
        moveShape();
        // проверка на границы
        checkFrames();

        Iterator<Shape[]> iterator = dropShapes.iterator();

        while (iterator.hasNext()) {
            Shape[] dropShape = iterator.next();

            for (Shape shape : dropShape) {
                Rectangle dropRectangle = shape.getRectangle();
                dropRectangle.y -= 100 * Gdx.graphics.getDeltaTime();
                shape.setRectangle(dropRectangle);
            }

            for (Shape savedSquare : savedShapes) {
                if (!check(dropShape, savedSquare)) {
                    continue;
                }

                iterator.remove();
                shapeGeneration();
                break;
            }

            // удаляем заполненные строки, если они есть
            deleteFullLines();
        }
    }

    /**
     * Возвращает сведения о том, можно ли считать падающую фигуру упавшей
     *
     * @param dropShapes - падающая фигура
     * @param savedShape - уже лежащая фигура
     * @return true, если фигура прекратила движение и сохранилась
     */
    private boolean check(Shape[] dropShapes, Shape savedShape) {
        for (Shape dropShape : dropShapes) {
            Rectangle dropRectangle = dropShape.getRectangle();

            if (dropRectangle.overlaps(savedShape.getRectangle())) {
                Shape[] shapes = savedShape.saveShape(dropShapes);

                for (Shape shape : shapes) {
                    savedShapes.add(shape);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * Метод, который удаляет заполненные строки
     */
    private void deleteFullLines() {
        int i = 0;

        while (i < array.length) {
            // проверяем, заполнена ли строка
            if (array[i] != 14) {
                // если нет, смотрим следующую
                i++;
                continue;
            }

            int j = 0;

            while (j < savedShapes.size) {
                Shape savedShape = savedShapes.get(j);
                Rectangle rectangle = savedShape.getRectangle();
                if (rectangle.y != (i + 3) * 32) {
                    j++;
                    continue;
                }

                // удаляем заполненную строку
                array[(int) rectangle.y / 32 - 3]--;
                savedShapes.removeValue(savedShapes.get(j), false);
                j = 0;
            }

            // смещаем вниз оставшиеся фигуры
            for (Shape savedShape : savedShapes) {
                Rectangle savedRectangle = savedShape.getRectangle();
                // если фигура ниже удаляемой строки, смотрим следующую
                if ((int) savedRectangle.y / 32 - 3 <= i) {
                    continue;
                }

                // перемещаем фигуру на строку ниже
                array[(int) savedRectangle.y / 32 - 3]--;
                array[(int) savedRectangle.y / 32 - 4]++;
                savedRectangle.y -= 32;
                savedShape.setRectangle(savedRectangle);
            }

            fullLines++;
            i = 0;
        }
    }

    private void moveShape() {
        Vector3 touchPos = new Vector3();
        touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(touchPos);

        if (!Gdx.input.justTouched()) {
            return;
        }

        if (touchPos.y <= 96) {
            if (touchPos.x <= 180) {
                for (Shape square : dropShapes.get(0)) {
                    Rectangle rectangle = square.getRectangle();
                    rectangle.x -= 32;
                    square.setRectangle(rectangle);
                }
                return;
            }

            if (touchPos.x >= 300) {
                for (Shape square : dropShapes.get(0)) {
                    Rectangle rectangle = square.getRectangle();
                    rectangle.x += 32;
                    square.setRectangle(rectangle);
                }
                return;
            }

            turnShape();
        }
    }

    private void turnShape() {
        Shape[] shape = dropShapes.get(0);
        switch (shape[0].getShapeType()) {
            case Strip: {
                turnStrip();
                break;
            }
            case Square: {
                break;
            }
            default: {
            }
        }
    }

    private void turnStrip() {
        Shape[] shape = dropShapes.get(0);

        Rectangle[] rectangles = {
                shape[0].getRectangle(),
                shape[1].getRectangle(),
                shape[2].getRectangle(),
                shape[3].getRectangle()
        };

        float newX = rectangles[2].x;
        float newY = rectangles[0].y;
        boolean isVertical = rectangles[0].x == rectangles[1].x;

        for (int i = 0; i < shape.length; i++) {
            if (!isVertical) {
                rectangles[i].x = newX;
                rectangles[i].y = newY - 32 * i;
            } else {
                switch (i) {
                    case 0: {
                        rectangles[i].x = newX - 64;
                        break;
                    }
                    case 1: {
                        rectangles[i].x = newX - 32;
                        break;
                    }
                    case 2: {
                        rectangles[i].x = newX;
                        break;
                    }
                    case 3: {
                        rectangles[i].x = newX + 32;
                        break;
                    }
                }

                rectangles[i].y = newY;
            }

            dropShapes.get(0)[i].setRectangle(rectangles[i]);
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
        // воспроизведение фоновой музыки
        // backgroundMusic.play();
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
        saved.dispose();
        squareImage.dispose();
        stripImage.dispose();
        //dropSound.dispose();
        //backgroundMusic.dispose();
        backgroundTexture.dispose();
    }

    private void checkFrames() {
        Shape[] shapes = dropShapes.get(0);
        Rectangle[] rectangles = {
                shapes[0].getRectangle(),
                shapes[1].getRectangle(),
                shapes[2].getRectangle(),
                shapes[3].getRectangle()
        };
        ShapeType shapeType = shapes[0].getShapeType();

        switch (shapeType) {
            case Square: {
                // левая граница
                if (rectangles[0].x <= 16 || rectangles[2].x <= 16) {
                    rectangles[0].x = 16;
                    rectangles[1].x = 48;
                    rectangles[2].x = 16;
                    rectangles[3].x = 48;
                }
                // правая граница
                if (rectangles[1].x >= 432 || rectangles[3].x >= 432) {
                    rectangles[0].x = 400;
                    rectangles[1].x = 432;
                    rectangles[2].x = 400;
                    rectangles[3].x = 432;
                }
                break;
            }
            case Strip: {
                boolean isVertical = rectangles[0].x == rectangles[1].x;
                // левая граница
                if (rectangles[0].x <= 16) {
                    rectangles[0].x = 16;
                    rectangles[1].x = isVertical ? 16 : 48;
                    rectangles[2].x = isVertical ? 16 : 80;
                    rectangles[3].x = isVertical ? 16 : 112;
                }
                // правая граница
                if (rectangles[3].x >= 432) {
                    rectangles[0].x = isVertical ? 432 : 336;
                    rectangles[1].x = isVertical ? 432 : 368;
                    rectangles[2].x = isVertical ? 432 : 400;
                    rectangles[3].x = 432;
                }
                break;
            }
            default: {
            }
        }

        for (int i = 0; i < shapes.length; i++) {
            dropShapes.get(0)[i].setRectangle(rectangles[i]);
        }
    }
}
